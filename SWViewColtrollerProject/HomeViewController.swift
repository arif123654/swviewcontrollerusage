//
//  HomeViewController.swift
//  SWViewColtrollerProject
//
//  Created by apple on 2/12/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
import  GooglePlaces
import GoogleMaps

class HomeViewController: UIViewController {

    //MARK:- Utilities
    @IBOutlet weak var menuNavigationButton: UIBarButtonItem!
    
    var ResturantCord = [[]]
    var MosqueCord = [[]]
    var HospitalCord = [[]]
    var PublicToiletCord = [[]]
    
    //MARK:- Init
    override func viewDidLoad() {
        super.viewDidLoad()
        menuNavigationButton.target = self.revealViewController()
        menuNavigationButton.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        
        //MARK:- Functions
        googleMap()
    }
    
    
    //MARK:- Helper Functions
    func googleMap(){
        
        let camera = GMSCameraPosition.camera(withLatitude: 23.731142, longitude: 90.416710, zoom: 18.9)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
       
    }
    
    

    
}




